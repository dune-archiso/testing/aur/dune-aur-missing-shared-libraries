FROM archlinux:base-devel

RUN pacman -Syyu --noconfirm gnu-free-fonts && \
  useradd --create-home arch && \
  passwd -d arch && \
  usermod -aG wheel arch && \
  echo 'arch  ALL=(ALL:ALL) ALL' >> /etc/sudoers

USER arch

RUN gpg --recv-keys 3F71FE0770D47FFB && \
  mkdir -p $HOME/AUR && \
  cd $HOME/AUR && \
  curl -Ls https://aur.archlinux.org/cgit/aur.git/snapshot/python-portalocker.tar.gz | tar -xvzf - -C $HOME/AUR && \
  cd python-portalocker && \
  makepkg -src --cleanbuild --noconfirm && \
  sudo pacman -U --noconfirm python-portalocker-*.zst && \
  cd $HOME/AUR && \
  curl -Ls https://aur.archlinux.org/cgit/aur.git/snapshot/dune-common.tar.gz | tar -xvzf - -C $HOME/AUR && \
  cd dune-common && \
  makepkg -src --cleanbuild --noconfirm && \
  sudo pacman -U --noconfirm dune-common-*.zst && \
  cd $HOME/AUR && \
  curl -Ls https://aur.archlinux.org/cgit/aur.git/snapshot/dune-geometry.tar.gz | tar -xvzf - -C $HOME/AUR && \
  cd dune-geometry && \
  makepkg -src --cleanbuild --noconfirm && \
  sudo pacman -U --noconfirm dune-geometry-*.zst && \
  rm -rf $HOME/AUR

WORKDIR $HOME
